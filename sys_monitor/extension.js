const GLib = imports.gi.GLib;
const ShellToolkit = imports.gi.St;
const Util = imports.misc.util;
const Gio = imports.gi.Gio;
const Main = imports.ui.main;

let timer;
var opt = "uname";
var extension_path = "./.local/share/gnome-shell/extensions/sys_monitor/scripts/";
let out;
let output = {};

let main_container_properties = { style_class: 'panel-button', reactive: true };
let main_container = new ShellToolkit.Bin(main_container_properties);
let main_container_content = new ShellToolkit.Label({ text: _get_values() });
let main_container_content_updater = function() { main_container_content.set_text(_get_values()); };
//const scripts = ['uname.sh', 'disk.sh', 'net_wan.sh'];

function _get_values()
{
	var command_output_bytes;

	switch (opt) {
		case "uname" :
			out = GLib.spawn_sync(null, ["bash",extension_path+"uname.sh","/"], null, GLib.SpawnFlags.SEARCH_PATH,null,null,null,output);
			command_output_bytes = new String(out[1]);
			break;
		case "disk" :
			out = GLib.spawn_sync(null, ["bash",extension_path+"disk.sh","/"], null, GLib.SpawnFlags.SEARCH_PATH,null,null,null,output);
			command_output_bytes = new String(out[1]);
			break;
		case "sys" :
			out = GLib.spawn_sync(null, ["bash",extension_path+"sys.sh"], null, GLib.SpawnFlags.SEARCH_PATH,null,null,null,output);
			command_output_bytes = new String(out[1]);
			break;
		case "cpu" :
			out = GLib.spawn_sync(null, ["bash",extension_path+"cpu.sh"], null, GLib.SpawnFlags.SEARCH_PATH,null,null,null,output);
			command_output_bytes = new String(out[1]);
			break;
		case "memory" :
			out = GLib.spawn_sync(null, ["bash",extension_path+"memory.sh"], null, GLib.SpawnFlags.SEARCH_PATH,null,null,null,output);
			command_output_bytes = new String(out[1]);
			break;
		case "net" :
			out = GLib.spawn_sync(null, ["bash",extension_path+"net.sh"], null, GLib.SpawnFlags.SEARCH_PATH,null,null,null,output);
			command_output_bytes = new String(out[1]);
			break;
	}

	var command_output_string;

	switch (opt) {
		case "uname" :
			command_output_string = 'Kernel : ';
			opt = "disk";
			break;
		case "disk" :
			command_output_string = 'Disk : ';
			opt = "sys";
			break;
		case "sys" :
			command_output_string = 'System : ';
			opt = "cpu";
			break;
		case "cpu" :
			command_output_string = 'CPU : ';
			opt = "memory";
			break;
		case "memory" :
			command_output_string = 'RAM : ';
			opt = "net";
			break;
		case "net" :
			command_output_string = '';
			opt = "uname";
			break;
		default:
			opt = "uname";
	}

	return command_output_string+command_output_bytes;
}

function init()
{
	main_container.set_child(main_container_content);
	main_container.connect('button-press-event', main_container_content_updater);
}

function enable()
{
	Main.panel._rightBox.insert_child_at_index(main_container, 0);
	timer = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 5000, () => {
        	main_container_content.set_text(_get_values());
        	return true; // Repeat
	});

}

function disable()
{
	Main.panel._rightBox.remove_child(main_container);
}
